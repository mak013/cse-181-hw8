from helpers import ReadFromFile
from math import sqrt

READ_PATH = "Problems/rosalind_ba8a.txt"
WRITE_PATH = "Answers/rosalind_ba8a.txt"

def EuclideanDistance(x,y):
	m = len(x)
	sigma = 0
	for i in xrange(m):
		sigma += pow(x[i]-y[i], 2)
	return sqrt(sigma)

def FarthestFirstTraversal(Data, k):
	Centers = [Data[0]]
	del Data[0]

	while len(Centers) < k:
		points = []
		# for each datapoint, find the center that minimize the distance
		# among all centers
		for Datapoint in Data:
			min_points = []
			for Center in Centers:
				min_points.append((Datapoint, EuclideanDistance(Datapoint, Center)))
			points.append(min(min_points, key=lambda i: i[1]))
		# find the datapoint that maximize the distance among all centers
		max_point = max(points, key=lambda i: i[1])
		Centers.append(max_point[0])
		Data.remove(max_point[0])
	return Centers

data = ReadFromFile(READ_PATH)
k = int(data[0].split()[0])
m = int(data[0].split()[1])
points = data[1:]

# organize points data in to x,y coordinates
data = []
for point in points:
	dimensions = []
	for dimension in point.split():
		dimensions.append(float(dimension))
	data.append(dimensions)
# Call the method
ans = FarthestFirstTraversal(data, k)

f = open(WRITE_PATH, "w")
for x in ans:
	f.write(str(x[0]) + " " + str(x[1]) + "\n")
f.close()