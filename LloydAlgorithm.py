from helpers import ReadFromFile, EuclideanDistance
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba8c.txt"
WRITE_PATH = "Answers/rosalind_ba8c.txt"

# parse the data from the file into a friendly format
def FriendlyData(path):
	data = ReadFromFile(path)
	k = int(data[0].split()[0])
	m = int(data[0].split()[1])
	points = data[1:]

	# organize points data in to x,y coordinates
	data = []
	for point in points:
		dimensions = []
		for dimension in point.split():
			dimensions.append(float(dimension))
		data.append(dimensions)

	return {"k": k, "data": data}

def LloydAlgorithms(k, Data): 
	centers = [data for data in Data[0:k]]
	clusters = defaultdict(list)


	''' Centers to Clusters '''
	# go through each data point and calculate which is the closet to the
	# center. If that's the closest, it goes to that center
	for datapoint in Data:
		distances = []
		for center in centers:
			distances.append(EuclideanDistance(datapoint, center))
		clusters[tuple(centers[distances.index(min(distances))])].append(datapoint)
	''' Cluster to Centers '''


data = FriendlyData(READ_PATH)
LloydAlgorithms(data["k"], data["data"])