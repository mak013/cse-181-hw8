from helpers import ReadFromFile, EuclideanDistance

READ_PATH = "Problems/rosalind_ba8b.txt"
WRITE_PATH = "Answers/rosalind_ba8b.txt"

def SquareErrorDistortion(Data, Centers):
	data_calculations = []
	# find the minimum for each datapoint among all centers
	for Datapoint in Data:
		min_points = []
		for Center in Centers:
			min_points.append((Datapoint, EuclideanDistance(Datapoint, Center)))
		data_calculations.append(min(min_points, key=lambda i: i[1]))
	# add to the sum after squaring each calculation
	sigma = 0
	for data_calculation in data_calculations:
		sigma += pow(data_calculation[1], 2)
	return sigma/len(data_calculations)

# to check for when the file splits from Data to Centers
marker_path = "--------"
marker = 0
data = ReadFromFile(READ_PATH)
k, m = [int(x) for x in data[0].split()]
# record Centers and Datapoints
centers = []
datapoints = []
# record centers first
for x in xrange(1, len(data)):
	if data[x] == marker_path:
		marker = x
		break;
	else:
		centers.append([float(m) for m in data[x].split()])
# record the datapoints second
for x in xrange(marker + 1, len(data)):
	datapoints.append([float(m) for m in data[x].split()])
# method call
ans = SquareErrorDistortion(datapoints, centers)

f = open(WRITE_PATH, "w")
f.write(str(ans))
f.close()